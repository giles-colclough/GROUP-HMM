classdef GaussianHMM < HMM
%GAUSSIANHMM train a MVN HMM
%
% HMM = GAUSSIANHMM(TRAINDATA, NCLASSES, INIT)
%
% The observation model is normal-wishart:
%  x ~ N(mu, (Lambda)^-1) for precision Lambda 
%  mu ~ N(Mu_0, (beta0 * Lambda)^-1) for scaling beta0
%  Lambda ~ Wi(a0, B0) for scale B0 where <Lambda> = a0 * B0.
%
% The approximate posterior has exactly the same form: it's normal-Wishart
%
% parameters mu, beta, a, B are a fixed update: given the soft state
% allocations and the data, this posterior is analytic. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% PROPERTIES
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    properties (GetAccess = protected, SetAccess = protected, Dependent = true, Hidden = true)
        obsLogLikelihood
	end
	
	properties (GetAccess = public, SetAccess = protected, Dependent = true, Hidden = true)
		stateCovarianceMatrices
	end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% METHODS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % CONSTRUCTOR
    % -----------------------------------------------------------------
    methods
        function self = GaussianHMM(varargin)

            % inherit constructor from superclass
            self@HMM(varargin{:})
        end%constructor
    end%methods

    % GET and SET METHODS
    % -----------------------------------------------------------------
    methods
        function obsLogLikelihood = get.obsLogLikelihood(self)
        % OBSLOGLIKELIHOOD
        % marginal likelihood of each datapoint, under each possible state,
        % given the priors. 
        % This function uses eq 11 fro Penny (2014) "Bayesian Inference for
        % the Multivariate Normal"
        % HAVE CAUTION over Penny's Wishart dist definition
        % Penny W(a,B) = wikipedia W(2a,(2B)^-1)
        %              =           W(n,  V)

        % declare memory
        obsLogLikelihood = zeros(self.nSamples, self.nClasses);

        % loop over models
        for k = 1:self.nClasses,
            stateModel = self.Posterior.ObsModel(k);
            
            cov_R = stateModel.wishartScaleMatrixInv_chol      ...
                    ./ sqrt(stateModel.precisionScale          ...
                            ./ (stateModel.precisionScale + 1) ...
                            .* (stateModel.wishartDoF - self.p + 1));
            v     = stateModel.wishartDoF - self.p + 1.0; % care over Penny's Wishart dof factor of 2
            
            % compute marginal log-pdf for each time point
            obsLogLikelihood(:,k) = gammaln(0.5 * (v + self.p))   ...
                                    - gammaln(0.5 * v)            ...
                                    - 0.5 * self.p * log(v)       ...
                                    - 0.5 * self.p * self.LOG_PI  ...
                                    - sum(log(diag(cov_R)))       ... % - 0.5 * logdet(Cov)
                                    - 0.5 * (v + self.p)          ...
                                    .* log(1 + sum(               ...
                                        (cov_R' \ bsxfun(@minus, self.X', stateModel.mean(:))).^2, ...
                                                   1).' ./ v);

% at the moment, this code makes things slower.
%              obsLogLikelihood(:,k) = t_log_pdf_mx(self.X, stateModel.mean, cov_R, v);

        end%for
        end%obsLogLikelihood
		
		function C = get.stateCovarianceMatrices(self)
			for k = self.nClasses:-1:1,
				R        = self.Posterior.ObsModel(k).wishartScaleMatrixInv_chol;
				C(:,:,k) = R'*R ./ self.Posterior.ObsModel(k).wishartDoF;
			end%for
		end%stateCovarianceMatrices
    end%methods 

    % INTEGRITY CHECKS
    % -----------------------------------------------------------------
    % methods hidden from the user, and which cannot be over-written, but which are inherited
    methods (Access = protected, Hidden = true, Sealed = true)
        function ret = check_obs_prior(self, ObsModel)
            function ret = checks(ObsModel)
                assert(isstruct(ObsModel) && isfield(ObsModel, 'wishartDoF')            ...
                                          && isfield(ObsModel, 'wishartScaleMatrixInv') ...
                                          && isfield(ObsModel, 'precisionScale')        ...
                                          && isfield(ObsModel, 'mean'));
                % parameters of normal-Wishart distribution
                assert(all(size(ObsModel.mean) == [1 self.p]));
                assert(isscalar(ObsModel.precisionScale) ...
                       && ObsModel.precisionScale > 0);
                   assert(isscalar(ObsModel.wishartDoF) ...
                       && ObsModel.wishartDoF > 0);
                assert(ismatrix(ObsModel.wishartScaleMatrixInv)            ...
                       && self.p == size(ObsModel.wishartScaleMatrixInv,1) ...
                       && self.isposdef(ObsModel.wishartScaleMatrixInv));
                ret = 0;
            end%checks

            % loop checks over every element
            assert(length(ObsModel) == self.nClasses);
            arrayfun(@checks, ObsModel);
            ret = 0;
        end%check_obs_prior

        function ret = check_obs_posterior(self, ObsModel)
            function ret = checks(ObsModel)
                assert(isstruct(ObsModel) && isfield(ObsModel, 'wishartDoF')            ...
                                          && isfield(ObsModel, 'wishartScaleMatrixInv_chol') ...
                                          && isfield(ObsModel, 'precisionScale')        ...
                                          && isfield(ObsModel, 'mean'));
                % parameters of normal-Wishart distribution
                assert(all(size(ObsModel.mean) == [1 self.p]));
                assert(isscalar(ObsModel.precisionScale) ...
                       && ObsModel.precisionScale > 0);
                   assert(isscalar(ObsModel.wishartDoF) ...
                       && ObsModel.wishartDoF > 0);
                assert(ismatrix(ObsModel.wishartScaleMatrixInv_chol)            ...
                       && self.p == size(ObsModel.wishartScaleMatrixInv_chol,1) ...
                       && istriu(ObsModel.wishartScaleMatrixInv_chol));
                ret = 0;
            end%checks

            % loop checks over every element
            assert(length(ObsModel) == self.nClasses);
            arrayfun(@checks, ObsModel);
            ret = 0;
        end%check_posterior
    end%methods
    
    % INFERENCE ENGINE
    % -----------------------------------------------------------------
    methods (Access = protected, Hidden = true)
        function self = obs_posterior_inference(self)

            Nk = sum(self.gamma, 1); % column sum
            for k = self.nClasses:-1:1,
                scaledData   = bsxfun(@times, self.X, self.gamma(:,k));
                xbar         = sum(scaledData,1) ./ Nk(k);
                demeanedData = bsxfun(@times,                       ...
                                      bsxfun(@minus, self.X, xbar), ...
                                      sqrt(self.gamma(:,k)));
                stateCov     = (demeanedData' * demeanedData) ./ Nk(k);
                beta         = self.Prior.ObsModel(k).precisionScale + Nk(k);
                meanDiff     = xbar - self.Prior.ObsModel(k).mean;
                meanSumSq    = meanDiff' * meanDiff; % this should be pxp

                
                self.Posterior.ObsModel(k).wishartDoF    = self.Prior.ObsModel(k).wishartDoF + Nk(k);
                self.Posterior.ObsModel(k).mean          = (self.Prior.ObsModel(k).mean .* self.Prior.ObsModel(k).precisionScale ...
                                                            + xbar .* Nk(k)) ...
                                                           ./ beta;
                self.Posterior.ObsModel(k).precisionScale = beta;
                % we store the cholesky factor of the inverse scale matrix,
                % as this needs to be passed to the marginal likelihood
                % function before heading to the forward-backward algorithm
                % if memory is a problem, store sparse lower factor.
                self.Posterior.ObsModel(k).wishartScaleMatrixInv_chol = chol(self.Prior.ObsModel(k).wishartScaleMatrixInv + Nk(k) .* stateCov ...
                                                                             + self.Prior.ObsModel(k).precisionScale * Nk(k) ./ beta .* meanSumSq);
            end%for
        end%obs_posterior_inference
    end%methods

    % DEFAULTS
    % -----------------------------------------------------------------
    methods (Access = protected, Hidden = true)

        function ObsModel  = default_obs_prior(self)
            for k = self.nClasses:-1:1,
                ObsModel(k).wishartDoF                 = 3;
                ObsModel(k).mean                       = zeros(1, self.p);
                ObsModel(k).precisionScale             = 1;
                ObsModel(k).wishartScaleMatrixInv      = eye(self.p);
            end%for
        end%default_obs_prior

        function self = init_obs_posterior(self)
            self = self.obs_posterior_inference();
        end%init_posterior
    end%methods

    % UTILS
    % -----------------------------------------------------------------
    methods (Access = protected, Hidden = true)
        function logPdf = t_log_pdf_inv_cov(self, X, mu, invCov_R, dof)
            % T_LOG_INV_PDF_INV_COV
            % log-pdf for a single datapoint under multivariate
            % t-distribution, with upper cholesky of inverse-covariance passed in. 
            % There is no argument checking here - beware
            logPdf = gammaln(0.5 * (dof + self.p)) ...
                     - gammaln(0.5 * dof)          ...
                     - 0.5 * self.p * log(dof)     ...
                     - 0.5 * self.p * self.LOG_PI  ...
                     + sum(log(diag(invCov_R)))    ... % 0.5 * logdet(invCov)
                     - 0.5 * (dof + self.p) * log( ...
                         1 + sum((invCov_R * (X(:) - mu(:))).^2) ./ dof);
        end%t_log_pdf_inv_cov
        function logPdf = t_log_pdf(self, X, mu, cov_R, dof)
            % T_LOG_INV_PDF_INV_COV
            % log-pdf for a single datapoint under multivariate
            % t-distribution, with upper cholesky of covariance passed in. 
            % There is no argument checking here - beware
           
            logPdf = gammaln(0.5 * (dof + self.p)) ...
                     - gammaln(0.5 * dof)          ...
                     - 0.5 * self.p * log(dof)     ...
                     - 0.5 * self.p * self.LOG_PI  ...
                     - sum(log(diag(cov_R)))       ... % - 0.5 * logdet(invCov)
                     - 0.5 * (dof + self.p) * log( ...
                         1 + sum((cov_R' \ (X(:) - mu(:))).^2) ./ dof);
        end%t_log_pdf_inv_cov
    end%methods

% WIP
% -------------------------------------

    methods (Access = public)
        function ret = test(self)
            ret = self.check();
        end%test
    end%methods
end%GaussianHMM