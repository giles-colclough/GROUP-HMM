# GROUP-HMM #

## A Bayeysian hierarchical model for the HMM with a Gaussian generative distribution ##

### An OO implementation for Matlab, with additional mex-files
### Giles Colclough, 2016
---
A toy problem for implementing a hierarchical HMM using VB at subject-level and EM at the top level. 

Released so-far: Matlab code for implementing the single-subject HMM. 

The whole project build on work published in van de Meent (2013) "Hierarchically-coupled hidden Markov models for learning rates from single-molecule data," 
but generalises to a multivariate emission model. 



Seriously work-in-progress
