
Init = struct();

% make 1-state Gaussian data
X  = randn(1000,10);
GH = GaussianHMM(X,5,Init,1);

clear rem
for i = 1:800,
    fprintf('%d\n', i);
    GH = GH.VB_step();
    for k = GH.nClasses:-1:1
        rem(k,i) = GH.Posterior.ObsModel(k).wishartDoF;
    end
end%for

% check convergence
plot(rem');

% find most likely state
[~,kMax] = max(mean(GH.gamma,1));

% find covariance of most likely state
t = GH.stateCovarianceMatrices;
display(t(:,:,kMax));

% have a look at state lifetimes
display(GH.meanStateLifetime);

% show most likely state
display(kMax)