// t_log_pdf_mx.hpp
// 21-01-2016 Giles Colclough

#ifndef __T_LOG_PDF_MX__
#define __T_LOG_PDF_MX__

#include "armadillo" /* statically linked to avoid errors in matlab */
#include <cmath>
#include "mex.h"

/* mathematical constants */
#ifndef M_PI
    #define M_PI  3.14159265358979323846
#endif
#ifndef M_lnSqrt2PI
    #define M_lnSqrt2PI 0.91893853320467274178
#endif

/* function declarations */
double gammaln(double x); 
arma::colvec t_log_pdf(arma::mat &X, const arma::rowvec &mu, const arma::mat &R, const double v);
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]);

#endif
/* EOF */