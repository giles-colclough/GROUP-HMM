function logPdf = t_log_pdf_mx(X, mu, R, nu)
% l = t_log_pdf_mx(X, mu, R, nu)
%   returns the log-pdf for each datapoint in X (n points x p dimensions)
%   using mean mu (1xp), cholesky factor of the covariance matrix R (pxp) and 
%   degrees of freedom nu.
% 

% Compiled successfully with this code
% /Applications/MATLAB_R2015a.app/bin/mex ./t_log_pdf_mx.cpp  -v -larmadillo -largeArrayDims  -I/usr/local/include/ -I./ CXXOPTIMFLAGS="-O0"
% This code should be optimal
% -DARMA_DONT_USE_WRAPPER - framework accelerate : don't use provided  blas/lapack from armadillo, use Matlab
% -march=native : use SSE3/4 instructions
% -std=c++14  
%
%  /Applications/MATLAB_R2015a.app/bin/mex ./t_log_pdf_mx.cpp  -v -DARMA_64BIT_WORD -DARMA_NO_WRAPPER CXXLIBS="\$CXXLIBS -framework Accelerate" -DARMA_NO_DEBUG -I/usr/local/include/ -I./ CXXOPTIMFLAGS="-O3 -DNDEBUG" CXXFLAGS="-fno-common -arch x86_64 -march=native -mmacosx-version-min=10.9 -fexceptions -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk -fobjc-arc -std=c++14 -stdlib=libc++" -DARMA_BLAS_LONG_LONG -I/usr/local/opt/armadillo/lib /usr/local/opt/armadillo/lib/libarmadillo.6.40.3.dylib

% /*
%  *	Copyright 2015 OHBA
%  *	This program is free software: you can redistribute it and/or modify
%  *	it under the terms of the GNU General Public License as published by
%  *	the Free Software Foundation, either version 3 of the License, or
%  *	(at your option) any later version.
%  *
%  *	This program is distributed in the hope that it will be useful,
%  *	but WITHOUT ANY WARRANTY; without even the implied warranty of
%  *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  *	GNU General Public License for more details.
%  *
%  *	You should have received a copy of the GNU General Public License
%  *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
%  *
%  *
%  *	$LastChangedBy$
%  *	$Revision$
%  *	$LastChangedDate$
%  *	Contact: giles.colclough@ohba.ox.ac.uk
%  *	Originally written on: MACI64 by Giles Colclough, 14-Jul-2015
%  */

error([mfilename ':NoMexFunction'], ...
      '%s: mex function must be compiled for this to work. \n', ...
      mfilename);