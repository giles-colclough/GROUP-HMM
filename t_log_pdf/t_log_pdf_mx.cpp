//
//  t_log_pdf_mx.cpp
//
//  Created by Giles Colclough on 20/01/2016.
//
//
//  Mex function for matlab interface to the log pdf for a multivariate 
//   t distribution

/*
 *	Copyright 2016 OHBA
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *	$LastChangedBy$
 *	$Revision$
 *	$LastChangedDate$
 *	Contact: giles.colclough@ohba.ox.ac.uk
 *	Originally written on: MACI64 by Giles Colclough, 14-Jul-2015
 */


#include "t_log_pdf_mx.hpp"


/* Logarithm of the gamma function.
 *  Returns NaN for negative arguments, even though log(gamma(x)) may
 *  actually be defined.
 *
 * Written by Tom Minka
 */
double gammaln(double x)
{
  static const double gamma_series[] = {
    76.18009172947146,
    -86.50532032941677,
    24.01409824083091,
    -1.231739572450155,
    0.1208650973866179e-2,
    -0.5395239384953e-5
  };
  
  int i;
  double denom, x1, series;
  if(x < 0)        return NAN;
  if(x == 0)       return INFINITY;
  if(!isfinite(x)) return x;
  
  /* Lanczos method */
  denom = x+1;
  x1 = x + 5.5;
  series = 1.000000000190015;
  for(i = 0; i < 6; i++) {
    series += gamma_series[i] / denom;
    denom += 1.0;
  }
  return( M_lnSqrt2PI + (x + 0.5) * std::log(x1) - x1 + std::log(series/x) );
}


/* t_log_pdf
 *  t_log_pdf(L, X, mu, R, v) fills L with the log-pdf of each time-point 
 *    (row) in X, based on upper-cholesky factor of covariance matrix R, 
 *    mean mu and dof v. 
 */
arma::colvec t_log_pdf(arma::mat &X, const arma::rowvec &mu, const arma::mat &R, const double v) {
    static const double LOG_PI = 1.144729885849400;
    
    auto p        = X.n_cols;    
    
    return(gammaln(0.5 * (v + p))           \
           - gammaln(0.5 * v)               \
           - 0.5 * p * std::log(v)          \
           - 0.5 * p * LOG_PI               \
           - arma::sum(arma::log(R.diag())) \
           - 0.5 * (v + p) * arma::log(1 +  \
               arma::sum(arma::pow(arma::solve(arma::trimatl(R.t()), (X.each_row() - mu).t()), 2), 0).t() \
               / v));

}

/* mexFunction(int num_outputs, mxArray* outputs, int num_inputs, mxArray* inputs)
 * l = t_log_pdf_mx(X, mu, R, nu)
 *   returns the log-pdf for each datapoint in X (n points x p dimensions)
 *   using mean mu (1xp), cholesky factor of the covariance matrix R (pxp) and
 *   degrees of freedom nu.
 */
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
    /* check for proper number of arguments */
    if(nrhs!=4) {
        mexErrMsgIdAndTxt("t_log_pdf_mx:nrhs","Four inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("t_log_pdf_mx:nlhs","One output required.");
    }
    
    /* assign inputs to variables */
    double* inX;        // input 1 - data
    double* inMu;       // input 2 - mean
    double* inR;        // input 3 - cholesky factor of covariance
    double  nu;         // input 4 - degrees of freedom
    size_t  nrows;
    size_t  ncols;
    size_t  nOutCols {1};
    double* logPdf;     // output 1
    
    inX  = (double*) mxGetPr(prhs[0]);
    inMu = (double*) mxGetPr(prhs[1]);
    inR  = (double*) mxGetPr(prhs[2]);
    nu   = mxGetScalar(prhs[3]);
    
    /* get dimensions of the input matrix */
    nrows = mxGetM(prhs[0]);
    ncols = mxGetN(prhs[0]);
    int nSamples {static_cast<int> (nrows)};
    int p        {static_cast<int> (ncols)};  
    
    /* Check dimensions match */
    if (1 != mxGetM(prhs[1]) && mxGetN(prhs[1]) != ncols) { 
        mexErrMsgIdAndTxt("t_log_pdf_mx:muDims", \
                          "mu should be a row vector as long as the number of columsn in X. \n");
    }
    if (mxGetM(prhs[2]) != ncols && mxGetN(prhs[2]) != ncols) {
        mexErrMsgIdAndTxt("t_log_pdf_mx:muDims", \
                          "R should be square, of same size as columns of X. \n");
    }
    
    /* create the output matrix */
    plhs[0] = mxCreateDoubleMatrix((mwSize)nrows, (mwSize) nOutCols, mxREAL);
    
    /* get a pointer to the real data in the output matrices */
    logPdf = mxGetPr(plhs[0]);
    
    /* assign input matrices to arma mat*/
    arma::mat    X (nSamples,p); //could also declare arma::mat X(nSamples,p)
    arma::mat    R (p,p);
    arma::rowvec mu (p);
    arma::uword  ii {0};
    for (auto index = 0; index < std::pow(p,2); index++) {
        ii    = index;
        R(ii) = inR[index];
    }
    for (auto index = 0; index < (nSamples * p); index++) {
        ii    = index;
        X(ii) = inX[index];
    }
    for (auto index = 0; index < p; index++) {
        ii     = index;
        mu(ii) = inMu[index];
    }
    
    /* Create output matrix */
    arma::colvec L(nSamples); // {arma::zeros<arma::colvec> (nSamples)};
    
    // Run t-pdf on each sample
    L = t_log_pdf(X, mu, R, nu);
    
    // Save out results
    for (int index = 0; index < nSamples; index++) {
        ii      = index;
        logPdf[index] = L(ii);
    }
    
    return;
}