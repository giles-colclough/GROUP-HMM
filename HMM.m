classdef (Abstract) HMM
%HMM an abstract class interface for running single-subject HMMs.
%
%
% Key properties
%
% HMM.X           - data
% HMM.nClasses
% HMM.gamma       - time courses of marginal state probabilities
% HMM.viterbiPath - most likely state path
% HMM.A           - expected state transition matrix
% HMM.pi_0        - expected initial state probabilities
% HMM.nSamples
% HMM.p           - number of variables in the model
% HMM.fSample
% HMM.Prior
% HMM.Posterior   - posterior parameters for each state
%
% Key methods
%
% HMM(TRAINDATA, NCLASSES, INIT) - constructor
% HMM.train()    - train the entire HMM
% HMM.VBstep()   - perform one VB iteration
% HMM.update_prior() - change the prior, e.g. using info from a hierarchy


%
% Some useful references
%
% http://www.cse.buffalo.edu/faculty/mbeal/thesis/beal03_3.pdf - for VB updates of states
%









%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% PROPERTIES
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % accessible properties set by the constructor and which are CONST
    properties (GetAccess = public, SetAccess = immutable)
        X        % data (p x nSamples)
        nClasses % number of classes in HMM
        nSamples % number of data points
        p        % number of variables
        fSample  % sample rate
        time
        creationTime % time the object was constructed
        randomSeed   
    end

    % accessible properties which are set in this class
    properties (GetAccess = public, SetAccess = private)
        % state model expected parameters under VB
        A           % state transition matrix     (exp(<logA>))
        pi_0        % initial state probabilities (exp(<logpi_0>))

        % state probabilities under VB
        gamma       % marginal state probabilities

        nIterations % number of VB iterations performed
    end

    % accessible properties which may be set in subclasses
    properties (GetAccess = public, SetAccess = protected)
        % These two structures have sub-fields, State and ObsModel
        % Because this is a CE model, both Prior and Posterior share the same parameter labels
        % The State structure contains Dirichlet parameter vectors for pi_0, rho, and Dirichlet
        % parameter vectors for each row of A, alpha. These vectors are the VB fitted parameters 
        % for the approximate distributions on A and pi_0. 
        Prior
        Posterior
        freeEnergy
    end

    % properties which are determined on the fly
    properties (GetAccess = public, SetAccess = protected, Dependent = true)
        viterbiPath % most likely state path
        hardPath    % hard path assignment from marginal state probabilities
    end
    
    % public properties which are computed on the fly, and not printed in
    % display(HMM)
    properties (GetAccess = public, SetAccess = private, Dependent = true, Hidden = true)
        meanStateLifetime % computed by sampling from posterior
    end

    % protected and hidden properties are used internally, but not available to the user
    properties (GetAccess = protected, SetAccess = protected, Dependent = true, Hidden = true)
        obsLikelihood % likelihood of each datapoint given each possible state
    end
    properties (Abstract, GetAccess = protected, SetAccess = protected, Dependent = true, Hidden = true)
        obsLogLikelihood % log-likelihood of each datapoint given each possible state
	end
	
	% inaccessible properties which are set by the constructor	
	properties (GetAccess = protected, SetAccess = immutable, Hidden = true)
		useMex;
	end

	% constant properties which are the same for any class member.
    properties (Constant, GetAccess = protected, Hidden = true)
        PI     = pi;
        LOG_PI = log(pi);
	end

	
	
	
	
	
	
	
	
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% METHODS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % CONSTRUCTOR
    % -----------------------------------------------------------------
    methods
        function self = HMM(trainData, nClasses, Init, fSample)
            %HMM class constructor
            %
            % HMM = HMM(TRAINDATA, NCLASSES, INIT)
            %
            % HMM = HMM(TRAINDATA, NCLASSES, INIT, FSAMPLE)
            %

            % the constructor should be able to deal with an instantiation
            % without inputs
            if ~nargin,
                trainData               = [];
                nClasses                = 0;
                Init.classProbabilities = [];
            else
                narginchk(3,4);
            end%if

			% check for mex files
			mex_file_exists = @(fileName) 3 == exist(fileName, 'file');
			if mex_file_exists('hidden_state_inference_mx'),
				self.useMex = true;
			else
				self.useMex = false;
			end%if
			
            % basic data attributes
            self.X                  = trainData;
            self.nClasses           = nClasses;
            [self.nSamples, self.p] = size(trainData);

            % if random seed is not empty, hold random seed and update
            % system generator
            if ~isfield('randomSeed', Init),
                self.randomSeed = round(sum(clock)*1e3);
            else
                self.randomSeed = Init.randomSeed;
            end%if
            if isempty(self.randomSeed),
                % leave as is
            elseif isscalar(self.randomSeed) && self.randomSeed >= 0,
                rng(self.randomSeed, 'simdTwister');
            else
                error([mfilename ':BadRandomSeed'],               ...
                      ['%s: the random seed should be empty or ', ...
                       'a positive integer. \n'], mfilename);
            end%if

            % initialisation of class probabilities
            if ~isfield('classProbabilities', Init),
                self.gamma = self.random_class_probabilities(nClasses, self.nSamples);
            else
                self.gamma = Init.classProbabilities;
            end%if

            % attributes of object
            self.creationTime = datestr(now);
            self.nIterations  = 0;
	        self.freeEnergy   = NaN;

            % temporal properties
            if nargin < 4 || ~exist('fSample', 'var'),
                fprintf('Using a unit sampling rate. \n');
                self.fSample = 1;
            else
                self.fSample = fSample;
            end%if
            self.time = (1:self.nSamples) ./ self.fSample;

            % prior parameters
            if isfield('Prior', Init),
                self.Prior = Init.Prior;
            else
                self.Prior = self.default_prior();
            end%if

            % initialise model elements
            self = self.init_posterior();
            self = self.dirichlet_expectation_inference();

            self.check();
        end%constructor
    end%methods

	
	
	
	
    % GET AND SET METHODS
    % -----------------------------------------------------------------
    methods % must have default properties
        function path = get.hardPath(self)
            %HARDPATH the most likely state at each timepoint
            % NB - the most likely entire state course is given by the Viterbi path.
            [~,path] = max(self.gamma,[], 2);
        end%get.hardPath

        function path = get.viterbiPath(self)
            %VITERBIPATH the most likely state timecourse
            fprintf('viterbi path not yet implemented. \n');
            path = self.hardPath;
        end%get.viterbiPath

        function freeEnergy = get_free_energy(self)
            fprintf('Free energy not yet implemented. \n');
            freeEnergy = NaN;
        end%freeEnergy

        function obsLikelihood = get.obsLikelihood(self)
            %OBSLIKELIHOOD probability of data given the current model and parameters
            obsLikelihood = exp(self.obsLogLikelihood);
            obsLikelihood(obsLikelihood < realmin) = realmin;
        end%obsLikelihood

       function T = get.meanStateLifetime(self)
           %MEANSTATELIFETIME computed by drawing 100 samples from posterior
           
           nDraws = 100;
           % draw 100 instances of the state transition matrix
           Trep = self.sample_lifetimes(nDraws);
           T    = mean(Trep, 2);
        end%meanStateLifetime

        function self = set.Prior(self, Prior)
            %PRIOR defines prior for the model.
            %  we may want to change this in a hierarchical model.
            if nargin < 2 || isempty(Prior),
                Prior = self.default_prior();
            end%if
            self.check_prior(Prior);
            self.Prior = Prior;
        end%set.Prior
    end%methods

	
	
	
	
	
	
	
    % KEY USER FUNCTIONS
    % -----------------------------------------------------------------
    methods (Access = public, Sealed = true) % can't be overwritten in subclasses
        function self = train(self)
            %TRAIN the HMM
            self.check();
            error('Training not yet implemented. \n');
        end%train

        function self = VB_step(self, getFE)
            %VB_STEP perform one round of VB updates
            %
            % SELF = VB_STEP(SELF, GETFE) updates all parameters. If GETFE
            %    is true, then also updates the free energy. 
            
            if nargin < 2, getFE = false; end
            
            % keep track of how many steps we've done!
            self.nIterations = self.nIterations + 1;
            
            % E-step
            self          = self.dirichlet_expectation_inference();
            [self, ~, Xi] = self.hidden_state_inference();

            % FREE ENERGY IS ONLY VALID NOW
            if getFE,
                self.freeEnergy = get_free_energy();
            else
                % make it clear that FE is not valid
                self.freeEnergy = NaN;
            end%if

            % M-step
            self          = self.obs_posterior_inference();
            self          = self.state_posterior_inference(Xi);
        end%VB_step

        function self = update_prior(self, Prior)
        %UPDATE_PRIOR changes the prior
        % differentiated from set method to make it clear that a major change is occuring
        % maybe that's idiotic.
        % still....
            self.Prior = Prior;
        end%update_prior

        function T = sample_lifetimes(self, nSamples)
            %SAMPLE_LIFEIMES sample from posterior of state lifetimes
            % T = SAMPLE_LIFETIMES(SELF,N) draw N samples of state lifetimes, 
            %   returning nStates x N matrix of lifetimes T.
            %
            % It is useful to know that p(decay @ t) = l exp(-lt)
            % so that p(decay between 0 and t) = 1 - exp(-lt)
            % and p(still alive after time t) = exp(-lt)
            % The expected survival rate is the expectation of t under this
            % kernel, giving <t> = 1/l. (i.e. mean lifetime is the inverse
            % of the rate constant.)
            %
            % We know A_kk, survival prob after time t.
            % log(A_kk) = - l ./ fs
            % <t> = - 1 ./ (log(A_kk) * fs);
            % 
            % This works by applying discrete sampling to a continuous
            % limit - it's not exact for a discrete HMM. 
            
            % draw samples of state transition matrix
            for iSample = nSamples:-1:1,
                selfProb     = self.sample_A_marginal(1); % probability of staying in same state in time dT
                T(:,iSample) = - 1.0 ./ (log(selfProb) .* self.fSample);
            end%for
	end%sample_lifetimes
	
	function p = sample_A_marginal(self, nSamples)
		%SAMPLE_A_MARGINAL samples the diagonal of the state transition
		% matrix from the posterior
		%
		% P = SAMPLE_A_MARGINAL(SELF,N) draws N samples of the probability
		%   that each state survives on each new sample
		a = diag(self.Posterior.State.alpha);
		b = sum(self.Posterior.State.alpha,2) - a;
		p = randbeta(repmat(a, 1, nSamples), repmat(b, 1, nSamples));
	end%sample_A_marginal

	function A = sample_A(self, nSamples)
		%SAMPLE_A samples state transition matrix from posterior
		% A = SAMPLE_A(SELF,N) draws N samples of the state transition matrix
		for iSample = nSamples:-1:1,
			Adraw          = randgamma(self.Posterior.State.alpha);
			A(:,:,iSample) = bsxfun(@rdivide, Adraw, sum(Adraw,2));
		end
	end%sample_A
	       
    end%methods

	
	
	
	
    % INTEGRITY CHECKS
    % -----------------------------------------------------------------
    % methods hidden from the user, and which cannot be over-written, but which are inherited
    methods (Access = protected, Hidden = true, Sealed = true)
        function ret = check(self)
            %CHECK determines integrity of object

            % all functions should error or return zero
            c(1) = self.check_basic_properties();
            c(2) = self.check_probabilities();
            c(3) = self.check_prior();
            c(4) = self.check_posterior();

            % count failures
            ret = sum(c);
            assert(0==ret);
        end%check

        function ret = check_basic_properties(self)
            assert(isnumeric(self.X));
            assert(ndims(self.X) <= 2);
            assert(all(size(self.X) == [self.nSamples, self.p]));
            if self.nSamples <= self.p, 
                warning([mfilename ':DataDimensions'],         ...
                        ['Are you sure you''ve got the data ', ...
                         'as timepoints x dimensions?\n']);
            end%if
            assert(isnumeric(self.fSample)      ...
                   && 1 == length(self.fSample) && self.fSample > 0);
            assert(isnan(self.freeEnergy) || ...
                   (isnumeric(self.freeEnergy) && length(self.freeEnergy) == 1));
            ret = 0;
        end%check_basic_properties

        function ret = check_probabilities(self)
            if ~isempty(self.gamma),
                assert(all(size(self.gamma) == [self.nSamples, self.nClasses]));
                assert(isnumeric(self.gamma));
                assert(all(self.gamma(:) <= 1 & self.gamma(:) >= 0));
                assert(all(self.row_sum(self.gamma) - 1 <= eps(3)));
            end%if

            assert(isrow(self.pi_0) && length(self.pi_0) == self.nClasses ...
                   && sum(self.pi_0) - 1 < 2*eps(1));

            assert(ismatrix(self.A) && all(size(self.A) == self.nClasses) ...
                   && all(1 == row_sum(self.A)));
            
            assert(all(size(self.obsLikelihood) == [self.nSamples, self.nClasses]));
            
            assert(all(  self.hardPath >= 1 ...
                       & self.hardPath <= self.nClasses));

            assert(all(  self.viterbiPath >= 1 ...
                       & self.viterbiPath <= self.nClasses));
            ret = 0;
        end%check_probabilities

        function ret = check_prior(self, Prior)
            if nargin < 2 || ~exist('Prior', 'var') || isempty(Prior),
                Prior = self.Prior;
            end%if

            assert(isstruct(Prior) && isfield(Prior, 'State') ...
                                   && isfield(Prior, 'ObsModel'));
            c(2) = self.check_state_prior(Prior.State);
            c(1) = self.check_obs_prior(Prior.ObsModel);
            ret = sum(c);
        end%check_prior

        function ret = check_posterior(self, Posterior)
            if nargin < 2 || ~exist('Posterior', 'var') || isempty(Posterior),
                Posterior = self.Posterior;
            end%if

            assert(isstruct(Posterior) && isfield(Posterior, 'State') ...
                   && isfield(Posterior, 'ObsModel'));
            c(2) = self.check_state_posterior(Posterior.State);
            c(1) = self.check_obs_posterior(Posterior.ObsModel);
            ret = sum(c);
        end%check_prior


        function ret = check_state_prior(self, State)
            assert(isstruct(State) && isfield(State, 'alpha') ...
                                   && isfield(State, 'rho'));
            assert(isnumeric(State.alpha)                     ...
                   && ismatrix(State.alpha)                   ...
                   && self.nClasses == self.rows(State.alpha) ...
                   && self.nClasses == self.cols(State.alpha));
            assert(all(State.alpha(:) >= 0));
            assert(isnumeric(State.rho)   ...
                   && isrow(State.rho)    ...
                   && self.nClasses == length(State.rho));
            assert(all(State.rho(:) >= 0));
            ret = 0;
        end%check_state_prior

        function ret = check_state_posterior(self, State)
            % sanity checks for prior are the same as for posterior
            ret = self.check_state_prior(State);
        end%check_state_posterior

    end%methods

    % templates for methods in subclasses
    methods (Abstract, Access = protected, Hidden = true)
        ret = check_obs_prior(self, ObsModel)
        ret = check_obs_posterior(self, ObsModel)
    end%methods

	
	
	
	
% DEFAULTS
% -----------------------------------------------------------------

    methods (Access = private, Hidden = true, Sealed = true)

        function DefaultPrior = default_prior(self)
            fprintf('Using default prior. \n');
            DefaultPrior.ObsModel = self.default_obs_prior();
            DefaultPrior.State    = self.default_state_prior();
        end%default_prior

        function State = default_state_prior(self)
            % we set a prior strength, indep. number of classes - see Beal thesis chp.3
            defaultDirichletStrength = 1; 

            % Dirichlet prior on pi, the probabilities of the first state
            State.rho = repmat(defaultDirichletStrength, 1, self.nClasses) ./ self.nClasses;

            % Separate Dirichlet prior on each row of transition matrix
            State.alpha = repmat(defaultDirichletStrength, self.nClasses, self.nClasses) ./ self.nClasses;
        end%default_state_prior

        function self = init_posterior(self)
            self.Posterior                         = struct();
            self.Posterior.State                   = self.Prior.State;
            self.Posterior.ObsModel(self.nClasses) = struct();
            self                                   = self.init_obs_posterior();
        end%init_posterior

        function gamma = random_class_probabilities(self, nClasses, nSamples)
            %random_class_probabilities generates random initialisation
            % RANDOM_CLASS_PROBABILITIES(NCLASSES, NSAMPLES) generates
            %   a nCLASSES x NSAMPLES matrix where the columns sum to unity
            randAllocation = rand(nSamples, nClasses);
            gamma          = bsxfun(@rdivide, randAllocation, ...
                                    self.row_sum(randAllocation));
        end%random_class_probabilities
    end%methods

    methods (Abstract, Access = protected, Hidden = true)
        ObsModel = default_obs_prior(self)
        ObsModel = init_obs_posterior(self)
    end%methods
	
	
	
	
	

    % INFERENCE ENGINE
    % -----------------------------------------------------------------
    methods (Access = protected, Hidden = true)


        function [self, alpha, Xi, B] = hidden_state_inference(self)
            %HIDDEN_STATE_INFERENCE using foward-backward propagation
            % [SELF,ALPHA,XI,B] = HIDDEN_STATE_INFERENCE(SELF)
            %   returns the updated object SELF, together with probabilities
            %   of each state given all previous data points, ALPHA, probabilities 
            %   of each state given all previous and future data points, XI, 
            %   and the likelihood of the data at each time point given each 
            %   given each possible state, B. 

            B = self.obsLikelihood();

			if self.useMex,
                [self.gamma, Xi, ~] = hidden_state_inference_mx(B, self.pi_0, self.A, 0); % final parameter is order of mar model. Set to zero unless things are changing massively. 
                alpha = NaN;
			else
				% declate memory
				scale = zeros(self.nSamples,1);
				alpha = zeros(self.nSamples,self.nClasses);
				beta  = zeros(self.nSamples,self.nClasses);

				% forward pass
				alpha(1,:) = self.pi_0 .* B(1,:); % pi_0 should be a row vector
				scale(1)   = sum(alpha(1,:));
				alpha(1,:) = alpha(1,:) / (scale(1) + eps(scale(1))); % care over division by 0

				for i=2:self.nSamples,
					alpha(i,:) = (alpha(i-1,:) * self.A) .* B(i,:);
					scale(i)   = sum(alpha(i,:));		   
					% P(X_i | X_1 ... X_{i-1})
					alpha(i,:) = alpha(i,:) / (scale(i) + eps(scale(i)));
				end%for

				scale(scale<realmin) = realmin;

				% backward pass
				beta(self.nSamples,:) = ones(1,self.nClasses) / (scale(self.nSamples) + eps(scale(self.nSamples)));
				for i=self.nSamples-1:-1:1,
					beta(i,:) = (beta(i+1,:) .* B(i+1,:)) * (self.A') / (scale(i) + eps(scale(i)));
					beta(i,beta(i,:)>realmax) = realmax;
				end%for

				% state probabilities
				gammaTmp   = (alpha.*beta);
				self.gamma = bsxfun(@rdivide, gammaTmp, self.row_sum(gammaTmp));

				% joint probabilities
				Xi = zeros(self.nSamples-1, self.nClasses^2);
				for i = 1:self.nSamples-1,
					t       = self.A .* (alpha(i,:)' * (beta(i+1,:).*B(i+1,:)));
					% Xi reshapes t, which is KxK, into K^2x1. 
					% Get back to KxK using Xi_unwrapped = reshape(Xi(i,:), self.nClasses, self.nClasses). 
					Xi(i,:) = t(:).' ./ (sum(t(:)) + eps(t(1)));
				end%for
			end%if use Mex function
        end%hidden_state_inference
        
        function self = state_posterior_inference(self, Xi)
            % Inference on approximate posterior for dirichlet distributions
            
            
            % Beal 2003 Eq 3.57 or van de Meent SI Eq 91
            countsOverTime             = reshape(squeeze(sum(Xi,1)), ...
                                                 self.nClasses, self.nClasses);  
            self.Posterior.State.alpha = countsOverTime + self.Prior.State.alpha;
            
            % Beal 2003 Eq 3.55 or van de Meent SI Eq 92
            self.Posterior.State.rho = self.Prior.State.rho + self.gamma(1,:);
            
        end%state_posterior_inference
        
        function self = dirichlet_expectation_inference(self)
            % transition matrix
            psiSum  = psi(sum(self.Posterior.State.alpha(:)));
            Anonorm = exp(psi(self.Posterior.State.alpha) - psiSum);
            self.A  = bsxfun(@rdivide, Anonorm, self.row_sum(Anonorm));
            
            % initial State
            psiSum      = psi(sum(self.Posterior.State.rho));
            pi_0_nonorm = exp(psi(self.Posterior.State.rho) - psiSum);
            self.pi_0   = pi_0_nonorm ./ sum(pi_0_nonorm);
        end%dirichlet_expectation_inference
    end%methods
    
    methods (Abstract, Access = protected, Hidden = true)
        self = obs_posterior_inference(self);
    end%methods


	
	
	
% UTILS
% -----------------------------------------------------------------

    % static methods do not depend on the class instantiation
    methods (Access = protected, Static = true, Hidden = true)
        function s = row_sum(x)
            % ROW_SUM   Sum for each row.
            % A faster and more readable alternative to sum(x,2).

            % unfortunately, this removes any sparseness of x.
            s = x*ones(size(x,2),1);
        end%row_sum

        function r = rows(x)
            %ROWS number of rows
            % A more readable alternative to size(x,1).
            r = size(x,1);
        end%rows

        function c = cols(x)
            %COLS number of columns
            % A more readable alternative to size(x,2).
            c = size(x,2);
        end%cols

        function b = isposdef(a)
            % ISPOSDEF   Test for positive definite matrix.
            %    ISPOSDEF(A) returns 1 if A is positive definite, 0 otherwise.
            %    Using chol is much more efficient than computing eigenvectors.
            [~,test] = chol(a);
            b = (test == 0);
        end%isposdef
    end
end%HMM

    